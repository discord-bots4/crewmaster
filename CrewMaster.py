#==========================================================================
#
#          FILE:  CrewMate.py
# 
#         USAGE:  python CrewMate.py
# 
#   DESCRIPTION:  Discord bot for Amoung Us
#
#       OPTIONS:  none
#  REQUIREMENTS:  python 3, discord.py python-dotenv
#          BUGS:  ---
#         NOTES:
#          Repo:  https://gitlab.com/discord-bots4/crewmaster
#        AUTHOR:  Jon Kelley (jonkelleytech@gmail.com)
#       COMPANY:  
#       VERSION:  0.0
#       CREATED:  01/07/2023
#      REVISION:  
#   REVISIED BY:  
#==========================================================================

import os
import discord

from dotenv import load_dotenv          #Needed to load data from env files
from discord.ext import commands        #needed to use discord comamands

#Initilize modules
load_dotenv()

#Retrive enviorment settings
BOT_TOKEN = os.getenv('DISCORD_TOKEN')


#Discord intents need for the bot
intents = discord.Intents.default()
intents.message_content = True

#Create bot
#bot = commands.Bot (command_prefix='/', intents=intents)
bot = commands.Bot ()

ghostModeFlag = True
dead_players = ['None']
theMaster = 'None'
autoMuteFlag = True
roomCode = '0000'


#==========================================================================
# Events
#==========================================================================

#==========================================================================
# Function: on_ready()
#
#Purpose: Logs when the server side of the bot connects to Discord servers
#
# Paramters: None
#==========================================================================
@bot.event
async def on_ready():
    print (f'We have logged in as {bot.user}')

#==========================================================================
# Function: on_voice_state_update()
#
# Purpose: will auto mute everyone in the same voice channel as theMaser
#           if the master is unset, function will end. If the master is
#           not in a voice chat, the master will be unset and the fuction
#           will exit
#
# Paramters: None
#==========================================================================
@bot.event
async def on_voice_state_update(member, before, after):
    global theMaster
    if (autoMuteFlag):
        #if theMaster caused the call
        if (member == theMaster):
            #If theMaster changed voice channels clear who is master
            if (before.channel != after.channel):
                theMaster = 'None'
            #if the master self muted/unmuted
            elif (before.self_mute != after.self_mute) :
                #mute and deafen everyone in the same channel as theMaster
                affected_members = member.voice.channel.members
                
                #remove the master from the list of affected members
                affected_members.remove(theMaster)
                
                #if ghost mode is enabled, remove the dead form the
                #affected list
                if (ghostModeFlag):
                    if (dead_players[0] != 'None'):
                        for ghost in dead_players:
                            if (ghost != theMaser):
                                affected_members.remove(ghost)
                    else:
                        affected_members.remove(dead_players)

                #Get the status of self mute from the master
                masterMute = member.voice.self_mute
                
                #if (masterMute == True)
                #auto deafen the master
                await theMaster.edit(deafen = masterMute)
                
                #mute and deafen everyone else in the channel
                for affected_member in affected_members:
                    await affected_member.edit(mute = masterMute, 
                                            deafen = masterMute)
        #else someone else changed stated=s   
        else:
            #is there is no master, do nothing
            if (theMaster == 'None'):
                return
            #else if theMaster disconnected from voice chat, clear theMaster
            elif (theMaster.voice.channel == 'None'):
                theMaster = 'None' 

#==========================================================================
# Commands
#==========================================================================

#==========================================================================
# Function: mute() 
#
# Purpose: Will mute and deafen all members in the current voice channel
#           the caller is connected to.
#
# Paramters: ctx: the contex in which the command was called
#==========================================================================
@bot.slash_command(name='mute',
     help='Will mute and deafen  all members in the current channel')
async def mute(ctx):
    #Check of in voice channel, if not print messge and end command
    if (ctx.author.voice == None):
        await ctx.respond (ctx.author.name + ' not in voice chat')
        return
    #Send message everyone in channel will be muted
    response = 'All members in ' + ctx.author.voice.channel.name \
               + ' will be muted. Shhhh!'

    await ctx.respond(response)
    
    members = ctx.channel.members
    #remove ghosts
    if (dead_players[0] != 'None'):
        for ghost in dead_players:
            members.remove(ghost)
    #Mute everyone else in channel    
    for member in members:
        await member.edit(deafen=True, mute=True)

#==========================================================================
# Function: unmute() 
#
# Purpose: Will unmute and undeafen all members in the current voice channel
#           the caller is connected to.                                                                                                                                 
#
# Paramters: ctx: the contex in which the command was called
#==========================================================================
@bot.slash_command(name='unmute',
      help='Will unmute and undeafen  all members in the current channel')
async def unmute(ctx):
    #Check of in voice channel, if not print messge and end command
    if (ctx.author.voice == None):
        await ctx.respond (ctx.author.name + ' not in voice chat')
        return
    #Send message everyone in channel will be muted
    response = 'All members in ' + ctx.author.voice.channel.name \
               + ' will be unmuted. Use your inside voices'
     
    await ctx.respond(response)
    #Mute everyone in channel
    members = ctx.channel.members
    for member in members:
        await member.edit(deafen=False, mute=False)

#==========================================================================
# Function: setMaster()
#
# Purpose: Will set the master player for the auto mute and unmute
#
# Paramters: 
#       ctx: the contex in which the command was called
#==========================================================================
@bot.slash_command (name='setmaster',
              help='Sets what user to watch for auto unmute and remute')
async def setMaster (ctx, mentions=None):
    global theMaster
    #check to see if someone was passed, if not the caller is set
    if (mentions == None):
        theMaster = ctx.author
    #Uses the passed user for the new master    
    else:
        for member in ctx.author.voice.channel.members:
            if (member.id == int(mentions[2:-1])):
                theMaster = member
    await ctx.respond(theMaster.name + ' is now the Master')

#==========================================================================
# Function: showMaster() 
#
# Purpose: Will display who the master is
#
# Paramters:
#       ctx: The context in which the command was callled
#==========================================================================
@bot.slash_command (name='showmaster',
              help='Shows who the master is')
async def showMaster(ctx):
    #check to see is master is set
    if (theMaster == 'None'):
        await ctx.respond ('The master is unset')
    else:
        await ctx.respond ('The master is ' + theMaster.name)
#==========================================================================
# Function: unsetMaser()
#
# Purpose: Clears the person set as the master
#
# Paramters: 
#       ctx: The context in which the command was callled
#==========================================================================        
@bot.slash_command (name='unsetmaster',
              help='Clears the person set as the master')
async def unsetMaster (ctx):
    global theMaster
    theMaster = 'None'
    await ctx.respond('There is no longer a Master')

#==========================================================================
# Function: autoMute()
#
# Purpose: Will toggle on and off auto muting
#
# Paramters:
#       ctx: The context in which the command was callled
#==========================================================================
@bot.slash_command (name='automute',
               help='Will toggle  on and off  auto muting')
async def autoMute (ctx):
    if (autoMuteFlag):
        autoMuteFlag = False
        await ctx.respond('Auto muting is now disabled')
    else:
        autoMuteFlag = True
        await ctx.respond('Auto muting is now enabled')

#==========================================================================
# Function: ghostMode()
#
# Purpose: Turns on and off Ghost Mode.
#
# Paramters: 
#       ctx: The context in which the command was callled
#==========================================================================
@bot.slash_command (name='ghostmode',
              help='Will toggle on and off Ghost Mode')
async def ghostMode(ctx):
    global ghostModeFlag
    if (ghostModeFlag):
        ghostModeFlag = False
        await ctx.respond('Ghost mode is now disabled')
    else:
        ghostModeFlag = True
        await ctx.respond ('Ghost mode is now enabled')

#==========================================================================
# Function: dead()
#
# Purpose: Marks the calling member as dead
#
# Paramters:
#       ctx: The context in which the command was callled
#==========================================================================
@bot.slash_command (name='dead',
              help='Marks the calling player as dead')
async def dead(ctx):
    global dead_players
    if (dead_players[0] == 'None'):
        dead_players[0] = ctx.author
    else:
        if(ctx.author in dead_players):
            return
        else:
            dead_players.append(ctx.author)
    await ctx.respond(ctx.author.name + ' is now dead.')

#==========================================================================
# Function: kill()
#
# Purpose: Allows the Master to mark others as dead
#
# Paramters: 
#       ctx: The context in which the command was callled
#       mentions: The player that the master want to mark as dead
#==========================================================================
@bot.slash_command (name='kill',
              help='Allows the master to mark other users as dead')
async def kill(ctx, mentions):
    global dead_players
    #get member from channel to kill
    for member in ctx.channel.members:
        if (member.id == int (mentions[2:-1])):
            dead_player = member
    #confirm the master called the function       
    if (ctx.author == theMaster):
        if (dead_players[0] == 'None'):
            dead_players[0] = dead_player
            await ctx.respond(ctx.author.name + ' is now dead')
        elif(dead_player in dead_players):
            await ctx.respond(ctx.author.name + ' was already dead')
            return
        else:
            dead_players.append(dead_player)
            await ctx.respond(ctx.author.name + ' is now dead')
    else:
        if (theMaster == 'None'):
            await ctx.respond ( 'Only the Master can kill other players.' +  
            'The maser is not selected.')
        else:
            await ctx.respond(ctx.author.name + ' is not the master. Only ' 
                + theMaster.name + ' can kill people')

#==========================================================================
# Function: reset()
#
# Purpose: Clear the dead list at the end of the round
#
# Paramters: 
#       ctx: The context in which the command was callled
#==========================================================================
@bot.slash_command (name='reset',
              help='Will clear the dead away for the start of a new round')
async def reset(ctx):
    global dead_players
    if (ctx.author == theMaster):
        for player in dead_players:
            dead_players.remove(player)
        dead_players= ['None']
    await ctx.respond('Everyone is alive. Imposters get ready to murder'
                + ' again, you know who you are.')

#==========================================================================
# Function: honorTheDead() 
#
# Purpose: Shows the list of the dead
#
# Paramters: 
#       ctx: The context in which the command was callled
#==========================================================================
@bot.slash_command (name='honorthedead',
              help='Shows the list of the dead')
async def honorTheDead(ctx):
    await ctx.respond('We honor the dead')
    if (dead_players[0] == 'None'):
        await ctx.respond ('No one has died...yet.')
    else:
        for dead_player in dead_players:
            await ctx.respond(dead_player.name)


#==========================================================================
# Function: SetRoomCode() 
#
# Purpose: Sets the current room code
#
# Paramters: 
#       ctx: The context in which the command was callled
#       newCode: the new room code
#==========================================================================
@bot.slash_command (name='setroomcode',
              help='Sets the current room code')
async def setRoomCode(ctx, newcode):
    global roomCode
    roomCode = newcode
    await ctx.respond ('New room code: ' + roomCode)

#==========================================================================
# Function: GetRoomCode() 
#
# Purpose: Gets the current room code
#
# Paramters: 
#       ctx: The context in which the command was callled
#==========================================================================
@bot.slash_command (name='getroomcode',
              help='Gets the current room code')
async def getRoomCode(ctx):
    global roomCode
    await ctx.respond ('The current room code is: ' + roomCode)


#run the bot
bot.run(BOT_TOKEN)
